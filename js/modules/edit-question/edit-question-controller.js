(function(){
    /*global angular*/

    angular
        .module('edit-question')
        .controller('editQuestionController', EditQuestionController);

    EditQuestionController.$inject = ['questionService', '$stateParams', '$location'];

    function EditQuestionController(questionService, $stateParams, $location) {
        'use strict';

        var vm = this,
            questionId;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };
        vm.updateQuestion = updateQuestion;
        vm.setFieldFocus = setFieldFocus;
        vm.cancel = cancel;

        init();

        function init(){
            questionId = $stateParams.QuestionId;


            questionService
                .getQuestionById(questionId)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.question = response;
            }
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

        function updateQuestion(){

            if(vm.loading){
                return;
            }

            vm.loading = true;

            var data = {
                text: vm.question.text,
                description : vm.question.detail
            };

            questionService
                .editQuestion(data, questionId)
                .then(function(){
                    $location.path('/questions/' + questionId);
                })
                .catch(function(reason){
                    vm.loading = false;
                });
        }

        function setFieldFocus(){
            vm.focusField = true;
            vm.showPrompt = false;
        }

        function cancel(){
            $location.path('/');
        }




    }
})();