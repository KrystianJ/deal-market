(function(){
    /*global angular*/
    angular.module('community', [
        'ngRoute',
        'ui.router',
        'ui.bootstrap',
        'communityAPI',
        'component-navbar',
        'component-notifications-menu',
        'component-action-menu',
        'component-footer',
        'component-local-tabs',
        'component-search-main',
        'component-question-item',
        'hashtagify',
        'questions',
        'search-questions',
        'ask-question',
        'edit-question',
        'edit-answer',
        'answers',
        'youtube-embed',
        'notification',
        'timeFilter',
        'wordPluralFilter',
        'focus',
        'LocalStorageModule',
        'ngCookies',
        'profile'
    ]);

    angular
        .module('community')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', 'ENV'];

    function config($stateProvider, $urlRouterProvider, $httpProvider, ENV) {
        'use strict';

        $urlRouterProvider.otherwise('/questions');

        if(ENV.name === 'local'){
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/js/modules/landing-feed/home.html',
                    controller: 'homeCtrl',
                    controllerAs: 'home'
                });
        }

        $stateProvider
            .state('questions', {
                url: '/questions',
                templateUrl: '/js/modules/questions/questions.html',
                controller: 'questionsCtrl',
                controllerAs: 'vm'
            })
            .state('question-edit', {
                url: '/questions/:QuestionId/edit',
                templateUrl: '/js/modules/edit-question/edit-question.html',
                controller: 'editQuestionController',
                controllerAs: 'vm'
            })
            .state('unanswered-questions', {
                url: '/questions/unanswered',
                templateUrl: '/js/modules/questions/questions.html',
                controller: 'unansweredQuestionsCtrl',
                controllerAs: 'vm'
            })
            .state('user-questions', {
                url: '/questions/user/:Username',
                templateUrl: '/js/modules/questions/questions.html',
                controller: 'userQuestionsCtrl',
                controllerAs: 'vm'
            })
            .state('search-questions', {
                url: '/questions/search?query',
                templateUrl: '/js/modules/search-questions/search-questions.html',
                controller: 'searchQuestionsCtrl',
                controllerAs: 'vm'
            })
            .state('ask', {
                url: '/questions/ask',
                templateUrl: '/js/modules/ask-question/ask-question.html',
                controller: 'askCtrl',
                controllerAs: 'vm'
            })
            .state('answer', {
                url: '/questions/:QuestionId',
                templateUrl: '/js/modules/answers/answer.html',
                controller: 'answersCtrl',
                controllerAs: 'vm'
            })
            .state('answer-edit', {
                url: '/questions/:QuestionId/answer/:AnswerId/edit',
                templateUrl: '/js/modules/edit-answer/edit-answer.html',
                controller: 'editAnswerController',
                controllerAs: 'vm'
            })
            .state('profile', {
                url: '/profile/:Username',
                templateUrl: '/js/modules/profile/profile.html',
                controller: 'profileCtrl',
                controllerAs: 'vm'
            });

        $httpProvider.interceptors.push('authInterceptorsService');
    }
})();