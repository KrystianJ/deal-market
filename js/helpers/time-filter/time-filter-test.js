describe('Time filter', function () {
    'use strict';

    var $filter;

    beforeEach(function () {
        module('timeFilter');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should return empty string if no time is specified', function () {
        var time , result;

        result = $filter('timeFilter')(time);

        expect(result).toEqual('');
    });

    it('should convert unix timestamp to 10s ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 10s ago
        time = time.setSeconds(time.getSeconds() - 10);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('10s');
    });


    it('should convert unix timestamp to 1s ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 1s ago
        time = time.setSeconds(time.getSeconds() - 1);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('1s');
    });

    it('should convert unix timestamp to 1m ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 1m ago
        time = time.setMinutes(time.getMinutes() - 1);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('1m');
    });

    it('should convert unix timestamp to 20m ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 20m ago
        time = time.setMinutes(time.getMinutes() - 20);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('20m');
    });

    it('should convert unix timestamp to 1h ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 1h ago
        time = time.setHours(time.getHours() - 1);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('1h');
    });

    it('should convert unix timestamp to 4h ago', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 4h ago
        time = time.setHours(time.getHours() - 4);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('4h');
    });

    it('should convert unix timestamp to yesterday', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 25h ago which
        // indicates yesterday
        time = time.setHours(time.getHours() - 25);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('yesterday');
    });

    it('should convert unix timestamp to yesterday', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 47h ago which
        // indicates yesterday
        time = time.setHours(time.getHours() - 47);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        expect(result).toEqual('yesterday');
    });

    it('should convert unix timestamp to Formatted Date', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 48h ago which
        // indicates 2 days ago
        time = time.setHours(time.getHours() - 48);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        var expectedFormat = moment.unix(time).format('MMM DD, YYYY');

        expect(result).toEqual(expectedFormat);
    });

    it('should convert unix timestamp to Formatted Date', function () {
        var time = new Date(),
            result;

        // Simulate Unix Timestamp created 72h ago which
        // indicates 3 days ago
        time = time.setHours(time.getHours() - 72);
        time = moment(time).unix();

        // Call parsing function
        result = $filter('timeFilter')(time);

        var expectedFormat = moment.unix(time).format('MMM DD, YYYY');

        expect(result).toEqual(expectedFormat);
    });


});