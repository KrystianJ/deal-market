(function(){
    /*global angular*/

    angular
        .module('questions')
            .controller('unansweredQuestionsCtrl', UnansweredQuestions);

    UnansweredQuestions.$inject = ['questionService', 'currentUserService'];

    function UnansweredQuestions(questionService, currentUserService) {
        'use strict';

        var vm = this;
        var currentUser = currentUserService.currentUser();
        vm.deleteQuestion = deleteQuestion;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };

        vm.localTabsData = {
            heading: 'UNANSWERED QUESTIONS',
            items: {
                active: {
                    active: false,
                    link: 'questions?sort=active',
                    text: 'Active'
                },
                unanswered: {
                    active: true,
                    link: 'questions/unanswered',
                    text: 'Unanswered'
                },
                my: {
                    active: false,
                    link: 'questions/user/' + currentUser.username,
                    text: 'My Questions'
                }
            }
        };

        ////////////////////////////////////

        init();

        function init(){

            questionService
                .getUnansweredQuestions()
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.questions = response;
            }
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

        function deleteQuestion(question, index){
            vm.questions.splice(index, 1);
            questionService
                .deleteQuestion(question.id)
                .then()
                .catch(onRequestError);
        }

    }
})();