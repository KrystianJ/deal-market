/*
 * Created by garyfox on 13/09/2016.
 * This is for the Ask A Question Spec. Tests Run Are Chrome Mobile :
 * Ask Successful Question
 * Validation For Minimum Characters,Question With No Tags
 */
var helpers = require('../../common/helpers.js');
var communityHomeShared = require('../../common/pages/CommunityHome');
var data = require('../../common/data.json');
var logger = require('../../common/log.js');
describe('Ask A Question Functionality For Mobile Emulation', function () {
    var testParams = [data.users.userA, data.users.userB];
    beforeEach(function () {
        browser.get(browser.baseUrl);
    });
    it('Ask Successful Question With Tags', function () {

        communityHomeShared
            .selectUser(testParams[0])
            .deepLinkToAskQuestion()
            .askSuccessfulQuestion(data.successfulQuestionTitle, data.successfulQuestionDetail)
            .verifyOnQuestionWithAnswerPage();
    });
    it('Ask Successful Question Without Tags', function () {
        communityHomeShared
            .selectUser(testParams[0])
            .deepLinkToAskQuestion()
            .askSuccessfulQuestion(data.questionTitleNoTags, data.questionBodyNoTags)
            .verifyOnQuestionWithAnswerPage();
    });
    it('Ask Unsuccessful Question', function () {
        communityHomeShared
            .selectUser(testParams[0])
            .deepLinkToAskQuestion().validateUnsuccessfulQuestion();
    });
    afterEach(function () {
        if (browser.params.questionID !== 0 && browser.params.questionID !== undefined) {
            var dbSettings = require('../../common/dbqueries.js')(browser.params.dbDetails);
            var connection = dbSettings.connect();
            helpers.removeQuestionById(dbSettings, browser.params.questionID);
            browser.params.questionID = 0;
            browser.sleep(2000).then(function () {
                logger.log(data.loggingLevel.INFO, 'Done Deleting DB Entries');
                connection.end();
            });
        }
    });

});
