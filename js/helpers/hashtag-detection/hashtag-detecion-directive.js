(function(){
    angular
        .module('hashtagify',[])
        .directive('hashtagify', ['$timeout', '$compile',
            function($timeout, $compile) {
                return {
                    restrict: 'A',
                    scope: {
                        uClick: '&userClick',
                        tClick: '&termClick',
                        bindModel: '=ngModel'
                    },
                    link: function(scope, element, attrs) {

                        var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/gi;

                         scope.$watch('bindModel', function(newVal){
                            if(newVal){
                                replaceText(newVal);
                            }
                        }, true);

                        function replaceText(text){
                            var newHtml = text;

                            //Replace mentions
                            newHtml = newHtml.replace(/(|\s)*@(\w+)/g, '$1<a ng-click="uClick({$event: $event})" class="hashtag">@$2</a>');

                            // Replace Hashtags
                            newHtml = newHtml.replace(/(^|\s)*#(\w+)/g, '$1<a ng-click="tClick({$event: $event})" class="hashtag">#$2</a>');


                            // Replace url lilnks
                            newHtml = newHtml.replace(urlPattern, '<a target="_blank" href="$&">$&</a>');



                            element.html(newHtml);

                            $compile(element.contents())(scope);
                        }
                    }
                };
            }
        ]);

})();