/**
 * Created by garyfox on 13/10/2016.
 */
var helpers = require('../../common/helpers.js');
var communityPageObjects = require('../../common/objects/CommunityHomeObjects.js');

var CommunityHomeMobile = function () {

    this.selectUser = function (user) {
        helpers.checkElementExistAndClick(communityPageObjects.txtUser(user.alias));
        browser.sleep(2000);
        return this;

    };
    this.deepLinkToAnswer = function (answer) {
        browser.get(browser.baseUrl + 'questions/' + answer);
        return require('../../common/pages/ViewQuestionWithAnswer');

    };
    this.deepLinkToAskQuestion = function () {
        browser.get(browser.baseUrl + 'questions/ask');
        return require('../../common/pages/AskQuestion');

    };

};
module.exports = new CommunityHomeMobile();
