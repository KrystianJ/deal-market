(function () {
    angular
        .module('wordPluralFilter',[])
        .filter('wordPluralFilter', wordPluralFilter);

    function wordPluralFilter() {
        return function (value, word, returnValue) {
            // Check if we need to return a value and a word eg 1 vote
            if(returnValue){
                if(value === 1){
                    return value + ' ' + word;
                }{
                    return value + ' ' + word + 's';
                }
            }
            else{
                if(value === 1){
                    return word;
                }{
                    return word + 's';
                }
            }
        };
    }

}());
