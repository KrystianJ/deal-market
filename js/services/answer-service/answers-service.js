(function(){
    angular
        .module('communityAPI')
        .service('answersService', answersService);

    answersService.$inject = ['$http', 'ENV', 'errorService'];

    function answersService($http, ENV, errorService) {

        var answersEndpoint = ENV.apiEndpoint + 'questions/';
        var audienceEndpoint = ENV.audienceEndpoint;

        return{
            getAnswers : getAnswers,
            postAnswer : postAnswer,
            getAnswerById : getAnswerById,
            acceptAnswer : acceptAnswer,
            updateAnswer : updateAnswer
        };

        ////////////////////////////////

        function getAnswers(questionId){
            
            var uri = answersEndpoint + questionId + '/answers' + audienceEndpoint;
            
            return $http
                .get(uri)
                .then(onGetAnswersSuccess)
                .catch(onGetAnswersError);

            function onGetAnswersSuccess(response){
                return response.data.content;
            }

            function onGetAnswersError(response){
                return errorService.parseRejectPromise(response);
            }
        }

        /////////////////////////////////
        
        function getAnswerById(questionId, answerId){
            var uri = answersEndpoint + questionId + '/answers/' + answerId + audienceEndpoint;

            return $http
                .get(uri)
                .then(onGetAnswerSuccess)
                .catch(onGetAnswerError);

            function onGetAnswerSuccess(response){
                return response.data.content;
            }

            function onGetAnswerError(response){
                return errorService.parseRejectPromise(response);
            }
        }


        ////////////////////////////////

        function postAnswer(questionId, data){

            var uri = answersEndpoint + questionId + '/answers';

            return $http
                .post(uri, data)
                .then(onPostAnswerSuccess)
                .catch(onPostAnswerError);

            function onPostAnswerSuccess(response){
                return response.data.content.id;
            }

            function onPostAnswerError(response){
                return errorService.parseRejectPromise(response);
            }
        }
        
        //////////////////////////////////
        
        function acceptAnswer(questionId, answerId, acceptValue){
            var uri = answersEndpoint + questionId + '/answers/' + answerId + '/accept';
            
            var data = {
                state: acceptValue
            };
            
            return $http
                .post(uri, data)
                .then(onAcceptAnswerSuccess)
                .catch(onAcceptAnswerError);
            
            function onAcceptAnswerSuccess(response){
                return response.data.content;
            }
            
            function onAcceptAnswerError(response){
                return errorService.parseRejectPromise(response);
            }
        }

        //////////////////////////////////

        function updateAnswer(questionId, answerId, answerText){
            var uri = answersEndpoint + questionId + '/answers/' + answerId + audienceEndpoint;

            var data = {
                text: answerText
            };

            return $http
                .patch(uri, data)
                .then(onAcceptAnswerSuccess)
                .catch(onAcceptAnswerError);

            function onAcceptAnswerSuccess(response){
                return response.data.content;
            }

            function onAcceptAnswerError(response){
                return errorService.parseRejectPromise(response);
            }
        }
        
    }
})();