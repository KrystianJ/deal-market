(function (){
    angular
        .module('communityAPI')
        .service('votingService', votingService);

    votingService.$inject = ['$http', '$q', 'ENV', 'errorService'];

    function votingService($http, $q, ENV, errorService) {

        var baseEndpoint = ENV.apiEndpoint + 'questions/';


        return{
            vote : vote
        };

        //////////////////////////////

        /**
         * Vote up or down depending on data
         * 1 vote up
         * -1 vote down
         * 0 un-vote
         * @param questionID
         * @param answerID
         * @param voteValue
         * @returns {Promise<U>}
         */
        function vote(questionID, answerID, voteValue){

            var votingEndpoint = baseEndpoint + questionID + '/answers/' + answerID + '/vote';

            if(voteValue > 1 || voteValue < -1){
                return $q.reject('Value not supported');
            }

            var data = {
                state: voteValue
            };

            return $http
                .post(votingEndpoint, data)
                .then(onPostSuccess)
                .catch(onPostError);

            function onPostSuccess(response){
                return response.data.content;
            }

            function onPostError(response){
                return errorService.parseRejectPromise(response);
            }
        }

    }
})();